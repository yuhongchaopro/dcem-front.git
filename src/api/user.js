/**
 * User
 */
import request from '@/utils/http'// 导入http中创建的axios实例
// import qs from 'qs'; // 根据需求是否导入qs模块 

const user = {

  user_content() {
    return request({
      url: '/user/',
      method: 'get',
    })
  },
  update (data) {
    return request({
        url: '/user/' + String(data.id) + '/',
        method: 'put',
        data: data
    })
},

  // 其他接口…………
}
export default user;