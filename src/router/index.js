import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import MonitorIndex from '../components/MonitorIndex.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/monitorIndex'

  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import('../views/Home.vue'),
    children: [
      {
        path: '/monitorIndex',
        name: 'MonitorIndex',
        component: MonitorIndex
      },
      
    ],
  },

]

const router = new VueRouter({
  routes
})

export default router
