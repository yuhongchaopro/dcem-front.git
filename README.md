# dcem-front
#### 作者：于洪超

#### 介绍
域名SSL证书监控系统-前端项目

#### 软件架构
软件架构说明：Vue-2/Element-2/Axios/Npm-6.14.17/Node-v14.20.0


#### 安装教程

1.  开发环境：将后台接口地址维护到env.development文件的VUE_APP_BASE_URL变量中，然后，首次使用npm install 安装项目依赖；其次，以后启动项目使用npm run serve启动开发环境服务即可；
2.  生产环境：见3/4/5
3.  首先，将后台接口地址维护到.env.production文件的VUE_APP_BASE_URL变量中，
4.  其次，使用npm run build编译命令，得到dist文件夹，
5.  最后，使用Nginx代理dist文件夹。
6.  备注：成功启动之后，必须先在数据库表app_adminconfig内增加1条记录，id自增长，enterprise_email_suffix填写您企业邮箱后缀，例
如situdata.com，配置之后，web端才可以使用xxx@situdata.com的邮箱进行发送邮件以及登录流程，第一个登陆dcem系统的人自动变为超级管
理员权限。
7.  后端项目地址：https://gitee.com/yuhongchaopro/dcem-api.git

#### 使用说明

1.  浏览器访问：http://localhost:8080/#/login 即可访问到DCEM系统登陆页
2.  参考个人博客地址：[个人博客地址](https://blog.csdn.net/qq_34668238/article/details/131803239?spm=1001.2014.3001.5501)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
